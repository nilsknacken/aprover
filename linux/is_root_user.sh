#!/bin/bash

# Purpose: Determine if current user is root or not
is_root_user(){
 if ! [ $(id -u) -eq 0 ]; then
   echo "You need to run this script as a root user."
   exit
 fi
}
export -f is_root_user
