#!/bin/bash
# This file should be placed in openssl/src
# Installed versions should be found in openssl/version
# with the binary at openssl/version/bin/openssl
# Keys and certificates are located in openssl/key_cert
#
# This script launches a new shell and starts an OpenSSL test server 
#
# Input parameters: $1 $2 $3
# $1 : Specifies the location of the OpenSSL binary to run
# $2 : Specifies the port to launch the server on
# $3 : The base directory for the OpenSSL servers
# $4 : Flag specifying if DTLS should be used
# $5 : Quotes string giving additional arguments to be passed on.

file=$1
PORT=$2
BASE_DIR=$3
DTLS=$4
SUITE_B=$5
OPTIONS=$6

DTLS_STR=""
DTLS_FLAG=""
if $DTLS; then
  DTLS_STR="DTLS"
  DTLS_FLAG="-dtls1_2"
fi

print_version() {
 path=$1
 version=${path%/bin/openssl}
 version=${version#${BASE_DIR}/}
 echo $version
}

version=$(print_version $file)
KEY_DIR=${BASE_DIR}/key_cert

mytitle="OpenSSL $version running $DTLS_STR at port: $PORT"
  
# Set title of terminal window
echo -e '\033]2;'$mytitle'\007'

echo -e "Starting $DTLS_STR server $version at port:  $PORT"
if [ "x$OPTIONS" != "x" ]; then  
  echo -e "With additional arguments: $OPTIONS"
fi

if $SUITE_B; then
  echo -e "In SUITE B mode"
  GENERAL_RUN="$file s_server -cert ${KEY_DIR}/ec_cert.pem -key ${KEY_DIR}/eckey.pem -accept $PORT -cipher ECDSA $OPTIONS"
else
  GENERAL_RUN="$file s_server -cert ${KEY_DIR}/general.crt -key ${KEY_DIR}/general.key -accept $PORT -pass file:${KEY_DIR}/pwd.pass $OPTIONS"
fi
# Start TLS or DTLS server
if $DTLS; then
  echo  "Using DTLS 1.2"
  $GENERAL_RUN -dtls1_2
  if [ $? -eq 1 ]; then
    # DTLS 1.2 not supported so use DTLS 1 instead
    echo -e '\0033\0143' # Clear the terminal
    echo -e "Starting $DTLS_STR server $version at port:  $PORT"
    echo "DTLS 1.2 not supported, using DTLS 1 instead"
    $GENERAL_RUN -dtls1    
  fi
else 
  # TLS
  $GENERAL_RUN  
fi

