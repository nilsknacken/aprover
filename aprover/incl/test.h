//
//  test.h
//  aprover
//
//  Created by Martin Andersson on 04/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Basic test functions used during development to test
 * elementary functionality of data structures.
 */

#ifndef aprover_test_h
#define aprover_test_h

#include <iostream>
#include <string>

// The SQLite3 library is avaiable from here:
// https://www.sqlite.org/
#include "sqlite3.h"

#include "classes.h"
#include "version.h"
#include "database.h"
#include "feature.h"
#include "probe.h"
#include "server.h"

// The rapidjson JSON parser is available from here:
// https://github.com/miloyip/rapidjson
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

// file2str
#include <fstream>
#include <string>
#include <cerrno>

// In order to use the dynamic librarires
#include <dlfcn.h>

const std::string TEST_HOST = "guest-ubuntu";
const int TEST_PORT = 4433;

// Functions to test different elementary aspects of the program
void all_tests(Server& s, std::ostream& = std::cout);
void all_tests(std::ostream& = std::cout);
void test_version_h(std::ostream& = std::cout);
void test_serialise(std::ostream& = std::cout);
void test_database(std::ostream& = std::cout);
void test_json(std::ostream& = std::cout);
void test_dylib(Server& s, std::ostream& = std::cout);
void test_c_dylib(Server& s, std::ostream& = std::cout);

// Specify which feature/probe to run
void test_probe(Server& s, const std::string&, std::ostream& = std::cout);

#endif
