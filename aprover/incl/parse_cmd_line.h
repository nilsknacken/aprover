//
//  parse_cmd_line.h
//  aprover
//
//  Created by Martin Andersson on 16/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Parse the command line and set relevant variables.
 * Library used to parse command line arguments is available from here:
 * http://tclap.sourceforge.net/
 */

#ifndef aprover_parse_cmd_line_h
#define aprover_parse_cmd_line_h

#include "tclap/CmdLine.h"
#include "tclap/Visitor.h"
#include <tclap/HelpVisitor.h>

#include "database.h"
#include "utils.h"
#include "version_constants.h"

void parse_command_line(int argc, const char * argv[],
                        std::string& host, int& lower_port, int& upper_port,
                        std::string& feature_input, bool& verbose, bool& list, bool& web,
                        bool& update, bool& stack, bool& stackOnly);
#endif
