//
//  server.h
//  aprover
//
//  Created by Martin Andersson on 09/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * The Server class represents the targeted server and has functions to
 * obtain and modify information about the server. The probes are passed the
 * targeted Server object which allows them to retrieve info and store results
 * about the server.
 */

#ifndef aprover_server_h
#define aprover_server_h

#include <algorithm>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <iomanip>
#include <thread>
#include <chrono>

#include "classes.h"
#include "feature.h"
#include "database.h"
#include "version_constants.h"
#include "utils.h"

const std::string LOG_DIR = "logs/";

// Defines the score subtracted when a feature strongly
// indicates that the version is not likely
// Defined as a negative value
const int EXCLUDE_SCORE = -100;

// Defines the threshold value for including the feature
// Defined as a negative value
const int EXCLUDE_LIMIT = -50;

// Holds information about how likely a version is
struct version_result_t {
  OPENSSL_VERSION version;
  int score;
};

// Holds information about if the server is affected by a feature
//
// The Feature has to be referenced with a pointer because the size of
// a Feature object is unknown at compile time.
struct probe_result_t {
  Feature* feature;
  int  status;
};

// Comparator function to compare the score of two version_result_t
bool score_cmp(version_result_t i, version_result_t j);

/*
 * server_error: error associated with the Server class
 * takes explanantion as argument
 */
class server_error : public std::logic_error
{
public:
  explicit server_error(const std::string& what_arg) throw() // c++0x // c++11 noexcept
  : logic_error(what_arg) {}
  
  explicit server_error(const char* what_arg) throw() // c++0x // c++11 noexcept
  : logic_error(what_arg) {}
};

// Class representing the targeted server
class Server
{
public:
  Server();
  Server(const std::string& host, const int port);
  Server(const Server&) = default;           // copy constr
  //Server(Server&&) = default;                // move constr // leave out for c++0x
  
  ~Server() throw(); // c++11 noexcept; 
  
  Server& operator=(const Server&) = default;  // copy assignment
  //Server& operator=(Server&&) = default;       // move assignment // leave out for c++0x
  
  // Set functions
  void set_host(const std::string& h) {host_ = h;}
  void set_port(const int p) {port_ = p;}
  void add_info(const std::string& i) {info_ += i;}
  void add_info(const std::string i, const std::string prefix)
    { info_ += format_string_for_output(i, prefix);}
  void set_minor_version(const unsigned char& v) {ssl_minor_version_ = v;}
  
  // Get functions
  std::string get_host() const {return host_;}
  int get_port() const {return port_;}
  std::string get_info() const {return info_;}
  std::vector<probe_result_t> get_probe_results() const {return probe_results_;}
  unsigned char get_minor_version() const {return ssl_minor_version_;}
  std::vector<Feature> get_features_probed() const;

  std::vector<OPENSSL_VERSION> get_most_likely_version() const;
  Feature get_next_feature() const;
  
  void print(std::ostream& = std::cout) const;
  void print_verbose(std::ostream& = std::cout) const;
  void print_web(std::ostream& = std::cout) const;
  void log(std::string filename = "");
  
  void add_probe_results(Feature*, int);
  void fingerprint(bool verbose, bool nacl = false);
  bool port_open() const;
  
  bool TLSv1_2_support;
  bool TLSv1_1_support;
  bool TLSv1_0_support;
  bool SSLv3_support;
  bool heartbeat_support;

private:
  std::string host_;
  int port_;
  std::string info_;
  std::vector<probe_result_t> probe_results_;
  std::vector<version_result_t> version_results_;
  unsigned char ssl_minor_version_;
  
  // Milliseconds to sleep between probes
  static const int sleep_milli_ = 100;
  
  void init_version_results();
  void print_probe_results(std::ostream& = std::cout, bool verbose = false) const;
};



#endif
