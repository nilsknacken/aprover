//
//  version.h
//  aprover
//
//  Created by Martin Andersson on 28/05/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Functions to comvert between the two OpenSSL version
 * representations, enum and string.
 */

#ifndef aprover_version_h
#define aprover_version_h

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


#include "version_constants.h"
#include "myexceptions.h"

OPENSSL_VERSION version_str2enum(const std::string&);
const std::string version_enum2str(const OPENSSL_VERSION&);


#endif