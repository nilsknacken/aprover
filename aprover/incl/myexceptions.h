//
//  myexceptions.h
//  aprover
//
//  Created by Martin Andersson on 28/05/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * General exceptions to be thrown.
 */
 
 

#ifndef aprover_myexceptions_h
#define aprover_myexceptions_h

#include <stdexcept>
#include <exception>


// General format error
class format_error : public std::runtime_error
{
public:
  explicit format_error(const std::string& what_arg) throw() // c++0x // c++11 noexcept
  : runtime_error(what_arg) {}
  
  explicit format_error(const char* what_arg) throw() // c++0x // c++11 noexcept
  : runtime_error(what_arg) {}
};



#endif
