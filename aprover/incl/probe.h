//
//  probe.h
//  aprover
//
//  Created by Martin Andersson on 08/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probe is the virtual base class for all the probes.
 * Only holds the basic functions to create, run, destroy and get 
 * general information about the probe. 
 * Actual probing techniques are implmented in the derived classes.
 */

#ifndef aprover_probe_h
#define aprover_probe_h

#include <stdexcept>
#include <string>
#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

#include "server.h"
#include "myexceptions.h"
#include "ssl_record_constants.h"
#include "c_probe_utils.h"

// Status of a probe
#define DISABLED     -1
#define NOT_AFFECTED 0
#define AFFECTED     1
#define FAILED       9

static std::string status_as_string(const int& status) {
  switch (status) {
    case DISABLED:
      return "Disabled";
    case NOT_AFFECTED:
      return "Not affected";
    case AFFECTED:
      return "Affected";
    case FAILED:
      return "Failed";
    default:
    return "Unknown status";
    break;
  }
}

// Error to be thrown for Probing errors
class probe_error : public std::logic_error
{
public:
  explicit probe_error(const std::string& what_arg) :
  logic_error(what_arg) {}
  
  explicit probe_error(const char* what_arg) :
  logic_error(what_arg) {}
};


// Abstract base class for the probing techniques
class Probe {
public:
  Probe() = default;
  
  virtual ~Probe() {}
  
  // Return 0 for a successful probe
  virtual int run(Server&, bool verbose) = 0;
  
  // Set functions
  void set_status(int a) { status_ = a; }
  
  // Get functions
  int get_status() const { return status_; }
  
protected:
  int status_;

};

// the types of the class factories
typedef Probe* create_t();
typedef void destroy_t(Probe*);

#endif
