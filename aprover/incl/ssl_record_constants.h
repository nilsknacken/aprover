//
//  openssl_record_enums.h
//  aprover
//
//  Created by Martin Andersson on 17/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Constants used in the SL/TLS protocol.
 * Mainly used by the probes implemented in C.
 */

#ifndef aprover_ssl_record_constants_h
#define aprover_ssl_record_constants_h

// Record types
#define SSL_RECORD_TYPE__CCS       0x14 // 20
#define SSL_RECORD_TYPE__ALERT     0x15 // 21
#define SSL_RECORD_TYPE__HANDSHAKE 0x16 // 22
#define SSL_RECORD_TYPE__APP_DATA  0x17 // 23
#define SSL_RECORD_TYPE__HEARTBEAT 0x18 // 24

// Versions
#define SSL_MAJOR_VERSION__SSL3_TLS 0x03
// minor versions
#define SSL_MINOR_VERSION__SSL3    0x00
#define SSL_MINOR_VERSION__TLS1_0  0x01
#define SSL_MINOR_VERSION__TLS1_1  0x02
#define SSL_MINOR_VERSION__TLS1_2  0x03
#define SSL_MINOR_VERSION__UNKNOWN 0xff

// Handshake types
#define SSL_HANDSHAKE_TYPE__HELLO_REQUEST       0x00 // 0
#define SSL_HANDSHAKE_TYPE__CLIENT_HELLO        0x01 // 1
#define SSL_HANDSHAKE_TYPE__SERVER_HELLO        0x02 // 2
#define SSL_HANDSHAKE_TYPE__CERTIFICATE         0x0b // 11
#define SSL_HANDSHAKE_TYPE__SERVER_KEY_EXCHANGE 0x0c // 12
#define SSL_HANDSHAKE_TYPE__CERTIFICATE_REQUEST 0x0d // 13
#define SSL_HANDSHAKE_TYPE__SERVER_DONE         0x0e // 14
#define SSL_HANDSHAKE_TYPE__CERTIFICATE_VERIFY  0x0f // 15
#define SSL_HANDSHAKE_TYPE__CLIENT_KEY_EXCHANGE 0x10 // 16
#define SSL_HANDSHAKE_TYPE__FINISHED 0x14 // 20

// Alert types
// First byte
#define SSL_ALERT_WARNING                 0x01 // 1
#define SSL_ALERT_FATAL                   0x02 // 2
// Second byte
#define SSL_ALERT_CLOSE_NOTIFY            0x00 // 0
#define SSL_ALERT_UNEXPECTED_MSG          0x0a // 10
#define SSL_ALERT_BAD_RECORD_MAC          0x14 // 20
#define SSL_ALERT_DECRYPTION_FAILED       0x15 // 21
#define SSL_ALERT_RECORD_OVERFLOW         0x16 // 22
#define SSL_ALERT_HANDSHAKE_FAILURE       0x28 // 40
#define SSL_ALERT_ILLEGAL_PARAMETER       0x2f // 47
#define SSL_ALERT_DECODE_ERROR            0x32 // 50
#define SSL_ALERT_PROTOCOL_VERSION        0x46 // 70
#define SSL_ALERT_INAPPROPRIATE_FALLBACK  0x56 // 86



#endif
