//
//  database.h
//  aprover
//
//  Created by Martin Andersson on 01/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * A SQLite3 database is used to store information about the
 * features available to probe for. 
 * The database (DB) has a table called 'Features' with the following data:
 *    - 'Name' a text string uniquely identifying the feature
 *    - 'Descritption' a text string describing the feature
 *    - 'Affected_list' a list of versions affected by the feature
 *        specified as a string.
 *    - 'Access_complexity' a metric for how hard it is to probe for the feature.
 *        Specified as an integer 1-3 (easy-hard)
 *    - 'Destructive' a metric for how obtrusive probing for the feature is.
 *        Specified as an integer 0-3 (unobtrusive-obtrusive)
 *    - 'Lib' a string specifying the name of the dynamic library to be used 
 *        for probing.
 *    - 'Confidence' an integer in the range [0,10] specifying how good of an
 *        indication the feature is of that the targeted server version belongs
 *        to the affected verisions. Is mainly relevant when the targeted version
 *        is not affected by the probing technique.
 *    - 'Dependency' a string specifying the name of the feature that 
 *        has to be run before the entry is run.
 *
 * At the start of the program the information in the database is updated based on the
 * JSON files in the 'json' directory.
 */

#ifndef __aprover__database__
#define __aprover__database__

#include <stdio.h>

#include <string>
#include <vector>
#include <iostream>

// The SQLite3 library is avaiable from here:
// https://www.sqlite.org/
#include "sqlite3.h"

#include "classes.h"
#include "version.h"
#include "feature.h"
#include "myexceptions.h"

//using string_table_t = std::vector<std::vector<std::string>>; // c++11
typedef std::vector<std::vector<std::string>> string_table_t; // c++0x

// Error to be thrown in database
class database_error : public std::logic_error
{
public:
  explicit database_error(const std::string& what_arg) :
  logic_error(what_arg) {}
  
  explicit database_error(const char* what_arg) :
  logic_error(what_arg) {}
};

// Static class representing the database
class Database
{
public:
  Database() = delete;
  ~Database() = delete;
  
  static bool open(const char*);
  static void close();
  
  // Add/update and remove features from the database
  static void feature_update(const Feature&);
  static void feature_remove(const Feature&);
  static void update_from_json();
  
  // Set functions
  static void set_max_destructive(int value) {max_destructive_ = value;}
  
  // Get / search functions
  static Feature search_name(const std::string&, bool regex = true);
  static std::vector<Feature> get_features_excluding_probed(std::vector<Feature>&);
  static std::vector<Feature> search_affected_excluding_probed(std::vector<OPENSSL_VERSION>&, std::vector<Feature>&);
  static int get_max_destructive() {return max_destructive_;}
  
  static void list_all_features(std::ostream& = std::cout);
  
private:
  static sqlite3* db;
  static int max_destructive_;
  
  static void init_db();
  
  static string_table_t ask(sqlite3_stmt*);
  
  static void check_for_error();
  };

Feature database_row2feature(std::vector<std::string>);

// returns true if the container v contains the element x
bool contains(const std::vector<OPENSSL_VERSION>& v, const OPENSSL_VERSION& x);


#endif /* defined(__db__database__) */
