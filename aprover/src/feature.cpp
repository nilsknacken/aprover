//
//  feature.cpp
//  db
//
//  Created by Martin Andersson on 01/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Feature is the class used to hold objects specifying a feature to probe for.
 * The features are initalised from the entries in the database which in turn can be
 * defined from JSON files.
 */


#include "feature.h"
using namespace std;


// Explicit initalisation
Feature::Feature(const string& name,
                 const string& description,
                 const vector<OPENSSL_VERSION>& affected_versions,
                 const int& access_complexity,
                 const int& destructive,
                 const string& lib,
                 const int& conf,
                 const string& dep)
: name_(name),
description_(description),
affected_versions_(affected_versions),
access_complexity_(access_complexity),
destructive_(destructive),
lib_(lib),
confidence_(conf),
dependency_(dep)
{
  // Check for errors
  if (name.length() == 0)
    throw feature_error("Name must be set");
}


// Initialise Feature based on given JSON file
Feature::Feature(const char* jsonfile) {
  string json = file2str(jsonfile);
  rapidjson::Document doc;
  doc.Parse(json.c_str());
  
  // 2. Access values by DOM.
  rapidjson::Value& name = doc["name"];
  rapidjson::Value& desc = doc["description"];
  rapidjson::Value& aff_ver = doc["affected_versions"];
  rapidjson::Value& acc_compl = doc["access_complexity"];
  rapidjson::Value& dest = doc["destructive"];
  rapidjson::Value& lib = doc["lib"];
  rapidjson::Value& conf = doc["confidence"];
  rapidjson::Value& dep = doc["dependency"];
  
  
  name_ = name.GetString();
  description_ =  desc.GetString();
  affected_versions_ = unserialise_versions(aff_ver.GetString());
  access_complexity_ =  acc_compl.GetInt();
  destructive_ = dest.GetInt();
  lib_ = lib.GetString();
  confidence_ = conf.GetInt();
  dependency_ = dep.GetString();
}

// Print a feature to given stream, cout by default
void Feature::print(ostream& stream) const {
  stream << "----- Feature: " << name_ << endl;
  stream << format_string_for_output(description_, "Info: ");
  stream << format_string_for_output(serialise_versions(affected_versions_),
                                     "Affected versions: ");
  stream << "Access complexity: " << access_complexity_ << endl;
  stream << "Destructive: " << destructive_ << endl;
  stream << "Lib: " << lib_ << endl;
  stream << "Confidence: " << confidence_ << endl;
  stream << "Dependency: " << dependency_ << endl;
}


// Loads the dynamic library and runs it
void Feature::probe(Server& s, bool verbose) {
  cout << "--- Probing " << s.get_host() << ":" << s.get_port();
  cout << " for feature: " << name_ << endl;
  
  try {
    // load the library
    char file[128];
    strcpy(file, "lib/");
    strcat(file, lib_.c_str());

    void* probe_library = dlopen(file, RTLD_LAZY);
    if (!probe_library) {
      stringstream ss;
      ss << "Cannot load library: " << dlerror() << '\n';
      throw probe_error(ss.str());
    }
    if (verbose) cout << "Loaded library: " << file << endl;
    
    // reset errors
    dlerror();
    
    // load the symbols
    create_t* create_probe = (create_t*) dlsym(probe_library, "create");
    const char* dlsym_error = dlerror();
    if (dlsym_error) {
      stringstream ss;
      ss << "Cannot load symbol create: " << dlsym_error << '\n';
      throw probe_error(ss.str());
    }
    
    destroy_t* destroy_probe = (destroy_t*) dlsym(probe_library, "destroy");
    dlsym_error = dlerror();
    if (dlsym_error) {
      stringstream ss;
      ss << "Cannot load symbol destroy: " << dlsym_error << '\n';
      throw probe_error(ss.str());
    }
    if (verbose) cout << "Loaded symbols 'create' and 'destroy'." << endl;
    
    // create an instance of the class
    Probe* probe = create_probe();
    if (verbose) cout << "Instance of the Probe class created. Ready to use class" << endl;
    
    // use the class
    int ret = probe->run(s, verbose);
    if (ret != 0) {
      cout << "--- probe failed" << endl;
      destroy_probe(probe);
      dlclose(probe_library);
      stringstream ss;
      ss << "Failed to run probe for: " << name_ << endl;
      throw probe_error(ss.str());
    }
    
    // Handle result of probing
    s.add_probe_results(this, probe->get_status());
    
    // destroy the class
    destroy_probe(probe);
    
    // unload the test library
    dlclose(probe_library);
  }
  catch(probe_error& e) {
    cerr << e.what() << endl;
    cerr << "*** Failed to probe for feature: " << name_ << endl;
    s.add_probe_results(this, FAILED);
    s.add_info(" --> The probe '" + this->get_name() + "' failed.\n\n");
    return;
  }
  cout << "--- Completed probing for feature: " << name_ << endl;
}


// Serialise the vector of OPENSSL_VERSIONs into a comma-separated string
// Long sequences of sequential versions are denoted 'first:last'
string serialise_versions(const vector<OPENSSL_VERSION>& v) {
  string s;
  if (v.size()) {
    bool sequence = false;
    vector<OPENSSL_VERSION>::const_iterator it;
    for (it = v.begin(); it != v.end()-1; ++it) {
      if (sequence) {
        if (int(*it)+1 != int(*(it+1))) {
          // next version is not in sequence
          sequence = false;
          s.append(":" + version_enum2str(*it) + ", ");
        }
      }
      else {
        s.append(version_enum2str(*it));
        if (int(*it)+1 == int(*(it+1)))
          // next version is in sequence
          sequence = true;
        else
          s.append(", ");
      }
    }
    if (sequence)
      s.append(":");
    s.append(version_enum2str(*it)); // no comma for last entry
  }
  return s;
}


// Convert the comma separated string to a vector of OPENSSL_VERSIONs
vector<OPENSSL_VERSION> unserialise_versions(const string& s) {
  vector<OPENSSL_VERSION> v;
  stringstream ss(s);
  string token;
  
  while (getline(ss, token, ',')) {
    // sequence of versions contains :
    size_t pos = token.find(":");
    if (pos == string::npos) {
      // just a single version
      v.push_back(version_str2enum(token));
    }
    else {
      // sequence of versions detected version1:version2
      OPENSSL_VERSION ver1 = version_str2enum(token.substr(0,pos));
      OPENSSL_VERSION ver2 = version_str2enum(token.substr(pos+1));
      for (int enumint = int(ver1); enumint <= int(ver2); ++enumint) {
        v.push_back(static_cast<OPENSSL_VERSION>(enumint));
      }
    }
    
    if (ss.peek() == ',' || ss.peek() == ' ')
      ss.ignore();
  }
  return v;
}

// Return all of the file content as a string
string Feature::file2str(const char *filename)
{
  ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(static_cast<size_t>(in.tellg()));
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], static_cast<long>(contents.size()));
    in.close();
    return(contents);
  }
  throw(errno);
}

vector<string> json_in_dir(const char* json_dir) {
  vector<string> jsons;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (json_dir)) != NULL) {
    while ((ent = readdir (dir)) != NULL) {
      char * pch = strstr (ent->d_name,".json");;
      if (pch != NULL) {
        jsons.push_back(ent->d_name);
      }
    }
    closedir (dir);
  } else {
    /* could not open directory */
    ostringstream oss;
    oss << "Feature::json_in_dir -- Could not open directory: '" << json_dir;
    oss << "' only using entries already in the database\n" << endl;
    string ewhat = oss.str();
    cerr << format_string_for_output(ewhat);
    //throw feature_error(ewhat);
  }
  return jsons;
}