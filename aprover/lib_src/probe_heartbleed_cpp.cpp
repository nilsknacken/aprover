//
//  probe_heartbleed.cpp
//  aprover
//
//  Created by Martin Andersson on 16/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for vulnerability of the Heartbleed bug, CVE-2014-0160.
 * If the server does not support the Heartbleed extension
 * then it will show as disabled in the summary.
 */

#include "probe.h"
#include "classes.h"
#include "server.h"
#include <stdlib.h>

using namespace std;

class Probe_Heartbleed : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int);

int Probe_Heartbleed::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  if (! s.heartbeat_support) {
    cout << " Server does not support the Heartbeat extension" << endl;
    s.add_info("probe_heartbleed -- Server does not support the Heartbeat extension\n");
    this->set_status(DISABLED);
    return 0;
  }
  
  stringstream ss;
  ss << s.get_host() << ":" << s.get_port();
  string host_port = ss.str();
  
  void *handle;
  int (*probe)(const char*, int);
  char *error;
  
  handle = dlopen("lib/probe_heartbleed_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_heartbleed_probe");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(host_port.c_str(), verbose);
  s.add_info("probe_heartbleed -- ");
  
  if (c_ret == 1) {
    // Vulnerable
    cout << "Server vulnerable to Heartbleed" << endl;
    s.add_info("Server vulnerable to Heartbleed\n");
    this->set_status(AFFECTED);
  }
  else if (c_ret == 0) {
    // Not vulnerable
    cout << "Server not vulnerable to Heartbleed" << endl;
    s.add_info("Server not vulnerable to Heartbleed\n");
    this->set_status(NOT_AFFECTED);
  }
  else if (c_ret == 9) {
    // Could not connect or Heartbeat not supported
    ret = 1;
    s.add_info("Could not connect to server.\n");
    throw probe_error("Could not connect to server.\n");
  }
  else if (c_ret == 10) {
    ret = 1;
    s.add_info("Received unexpected record.\n");
    throw probe_error("Received unexpected record.\n");
  }
  else {
    cerr << "probe_heartbleed.cpp -- Unknown return code " << c_ret << endl;
    throw probe_error("Unknown return code from library\n");
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new Probe_Heartbleed;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

