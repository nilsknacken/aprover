//
//  probe_TLS1_SHA256.cpp
//  aprover
//
//  Created by Martin Andersson on 17/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for support of TLS 1.0 and a SHA256 cipher suite in the same connection.
 * Only OpenSSL 1.0.1 (erroneously) supports such a connection.
 */

#include "probe.h"
#include "classes.h"
#include "server.h"
#include <stdlib.h>

using namespace std;

class Probe_TLS1_SHA256 : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, int);

int Probe_TLS1_SHA256::run(Server& s, bool verbose) {
  (void)verbose;
  int ret = 0;
  this->set_status(FAILED);
  
  void *handle;
  int (*probe)(const char*, int, int);
  char *error;
  
  handle = dlopen("lib/probe_TLS1_SHA256_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_probetls1_sha256");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret;
  if (s.TLSv1_0_support) {
    if (s.TLSv1_2_support) {
      c_ret = probe(s.get_host().c_str(), s.get_port(), verbose);
    }
    else {
      c_ret = 7; // SHA256 was introduced in TLS 1.2
    }
  }
  else {
    c_ret = 8; // TLS1.0 not supported
  }
  
  s.add_info("probe_TLS1_SHA256 -- ");
  
  if (c_ret == 1) {
    // Vulnerable
    cout << "Handshake completed succesfully with TLS1.0 and a SHA256 cipher suite" << endl;
    s.add_info("Handshake completed succesfully with TLS 1.0 and a SHA256 cipher suite\n");
    this->set_status(AFFECTED);
  }
  else if (c_ret == 0) {
    // TLS1.0 and SHA256 ciphers not supported
    cout << "TLS 1.0 and SHA256 ciphers are not supported together." << endl;
    s.add_info("TLS 1.0 and SHA256 ciphers are not supported together.\n");
    this->set_status(NOT_AFFECTED);
  }
  else if (c_ret == 7) {
    // "SHA256 ciphers not supported."
    cout << "SHA256 ciphers not supported." << endl;
    s.add_info("SHA256 ciphers not supported.\n");
    this->set_status(DISABLED);
  }
  else if (c_ret == 17) {
    // "SHA256 ciphers not supported by us."
    cout << "SHA256 ciphers could not be selected." << endl;
    s.add_info("SHA256 ciphers could not be selected.\n");
    this->set_status(FAILED);
  }
  
  else if (c_ret == 8) {
    // "TLS 1.0 not supported by server."
    cout << "TLS 1.0 not supported by server.\n";
    s.add_info("TLS 1.0 not supported by server.\n");
    this->set_status(DISABLED);
  }
  else if (c_ret == 9) {
    // Could not connect to server
    ret = 1;
    s.add_info("Could not connect to server.\n");
    throw probe_error("Could not connect to server.\n");
  }
  else {
    ret = 1;
    cerr << "probe_TLS1_SHA256.cpp -- Unknown return code " << c_ret << endl;
    throw probe_error("Unknown return code from scripts/TLS1_SHA256.sh\n");
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new Probe_TLS1_SHA256;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

