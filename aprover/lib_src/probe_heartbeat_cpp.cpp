//
//  probe_heartbeat.cpp
//  aprover
//
//  Created by Martin Andersson on 08/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Checks for support of the heartbeat extension
 */

#include "probe.h"
#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>

using namespace std;

class C_Probe_Heartbeat : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, int);

int C_Probe_Heartbeat::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  void *handle;
  int (*probe)(const char*, int, int);
  char *error;
  
  handle = dlopen("lib/probe_heartbeat_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_heartbeat_probe");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(s.get_host().c_str(), s.get_port(), verbose);
  s.add_info("probe_heartbeat -- ");
  
  if (c_ret == 0) {
    cout << "Heartbeat extension not enabled." << endl;
    s.add_info("Heartbeat extension not enabled.\n");
    s.heartbeat_support = false;
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 1) {
    cout << "Heartbeat extension enabled." << endl;
    s.add_info("Heartbeat extension enabled.\n");
    s.heartbeat_support = true;
    this->set_status(AFFECTED);
  }
  else if (c_ret == 6) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == 7) {
    // SSL record failure
    ret = 1;
    s.add_info("SSL record declared negative data size.\n");
    throw probe_error("SSL record declared negative data size.\n");
  }
  else if (c_ret == 8) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 9) {
    // Lookup failure
    ret = 1;
    s.add_info("Could not look up host.\n");
    throw probe_error("Could not look up host.\n");
  }
  else {
    ret = 1;
    s.add_info("Unknown return code from library.\n");
    throw probe_error("Unknown return code from library.\n");
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new C_Probe_Heartbeat;
}

extern "C" void destroy(Probe* p) {
  delete p;
}
