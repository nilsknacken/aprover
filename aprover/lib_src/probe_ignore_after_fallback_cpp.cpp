//
//  probe_ignore_after_fallback.cpp
//
//
//  Created by Martin Andersson on 07/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Some versions ignores ciphers suites following the TLS_FALLBACK_SCSV in the ClientHello message.
 * By starting to list TLS_FALLBACK_SCSV in the cipher list this results in a handshake failure
 * on affected servers. Unaffected servers will respond with ServerHello.
 */

#include "probe.h"
#include "ssl_record_constants.h"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class C_Probe_Ignore_After_Fallback : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, int);

int C_Probe_Ignore_After_Fallback::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  void *handle;
  int (*probe)(const char*, int, int);
  char *error;
  
  handle = dlopen("lib/probe_ignore_after_fallback_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_probe_ignore_after_fallback");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(s.get_host().c_str(), s.get_port(), verbose);
  s.add_info("probe_ignore_after_fallback -- ");
  
  if (c_ret == 0) {
    cout << "Not affected, handled TLS_FALLBACK_SCSV correctly" << endl;
    s.add_info("Not affected, handled TLS_FALLBACK_SCSV correctly\n");
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 1) {
    cout << "Affected, cipher suites following TLS_FALLBACK_SCSV were ignored" << endl;
    s.add_info("Affected, cipher suites following TLS_FALLBACK_SCSV were ignored\n");
    this->set_status(AFFECTED);
  }
  else if (c_ret == 6) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == 7) {
    // SSL record failure
    ret = 1;
    s.add_info("SSL record declared negative data size.\n");
    throw probe_error("SSL record declared negative data size.\n");
  }
  else if (c_ret == 8) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 9) {
    // Lookup failure
    ret = 1;
    s.add_info("Could not look up host.\n");
    throw probe_error("Could not look up host.\n");
  }
  else {
    ret = 1;
    s.add_info("Unknown return code from library.\n");
    throw probe_error("Unknown return code from library.\n");
  }
  
  return ret;
}


// the class factories
extern "C" Probe* create() {
  return new C_Probe_Ignore_After_Fallback;
}

extern "C" void destroy(Probe* p) {
  delete p;
}
