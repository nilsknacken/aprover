//
//  probe_zero_ext.c
//
//
//  Created by Martin Andersson on 03/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for the commit found here:
 * https://github.com/openssl/openssl/commit/fe64245aa1b1f5519ddfe11e3c9d7ad49ae4de95
 *
 * Unaffected versions allow for a zero length extension block in the
 * ClientHello message. Affected versions will instead respond
 * with a DECODE_ERROR alert.
 *
 * Affects version: 1.0.1n and 1.0.2b
 *
 * Returns 0 if server accept zero length extension block.
 * Returns 1 if server does not accept zero length extension block.
 * Higher return codes are resevered for errors. See c_probe_utils.h.
 *
 * Loaded by: probe_zero_ext.cpp
 */

#include "c_probe_utils.h"

// Return codes
#define EXIT_NOT_AFF             0
#define EXIT_AFF                 1


// Probes the server for the feature
// See description at top of file for return codes used.
int c_zero_ext_probe(const char* host, int port, int verbose)
{
  int ret = EXIT_UNKNOWN_FAILURE; // return value
  int     err;

  int     sock;
  struct sockaddr_in server_addr;

  // Set up a TCP socket
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_zero_ext -- ERROR: Connect of TCP socket failed.\n");
    return EXIT_CONNECT_FAILURE;
  }

  // Get random bytes
  unsigned char* rand_bytes = (unsigned char*)malloc(sizeof(unsigned char)*28);
  RAND_bytes(rand_bytes, 28);
  
  // Get seconds since epoch
  time_t rawtime = time(NULL); // now
  
  unsigned char* epoch_time = (unsigned char*)malloc(sizeof(unsigned char)*4);
  //unsigned long n = rawtime;
  epoch_time[0] = (rawtime >> 24) & 0xFF;
  epoch_time[1] = (rawtime >> 16) & 0xFF;
  epoch_time[2] = (rawtime >> 8) & 0xFF;
  epoch_time[3] = rawtime & 0xFF;
  
  // Debug info
  if (verbose) {
    printf("## INFO: rand bytes");
    print_hex(rand_bytes, 28);
    
    printf("\n## INFO: epoch time (%ju sec)", rawtime);
    print_hex(epoch_time, 4);
    printf("\n");
  }
  
  unsigned char mod_data[8] = {
    SSL_HANDSHAKE_TYPE__CLIENT_HELLO, // type
    0x00, 0x00, 0xe0, // size
    SSL_MAJOR_VERSION__SSL3_TLS, // client major version
    SSL_MINOR_VERSION__TLS1_2, // client minor version
    // timestamp, randbytes, session, ciphers, compression will go in between here
    0x00, 0x00 // extensions length
  };
  size_t clienthello_data_mod_size = 228;
  unsigned char* clienthello_data_mod = (unsigned char*)malloc(sizeof(unsigned char)*clienthello_data_mod_size);
  memset(clienthello_data_mod, 0, clienthello_data_mod_size);
  
  // Construct clienthello
  memcpy(&clienthello_data_mod[0], &mod_data[0], 6);
  memcpy(&clienthello_data_mod[6], epoch_time, 4);
  memcpy(&clienthello_data_mod[10], rand_bytes, 28);
  memcpy(&clienthello_data_mod[38], clienthello_data2, 188);
  memcpy(&clienthello_data_mod[226], &mod_data[6], 2);
  
  unsigned char clienthello_mod_header[5] = {SSL_RECORD_TYPE__HANDSHAKE,
    SSL_MAJOR_VERSION__SSL3_TLS,
    SSL_MINOR_VERSION__TLS1_0,
    0x00, 0xe4}; // size = 228
  
  if (verbose) {
    printf("probe_zero_ext -- sending clienthello\n");
    //print_hex(clienthello_data_mod, clienthello_data_mod_size);
  }
  
  // Write client_hello
  write(sock, clienthello_mod_header, clienthello_header_size);
  write(sock, clienthello_data_mod, clienthello_data_mod_size);
  
  // Handle response
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size <= 0) {
    ret = EXIT_HANDSHAKE_FAILURE;
  }
  else if (record.type == SSL_RECORD_TYPE__ALERT &&
           record.subsubtype == SSL_ALERT_DECODE_ERROR) {
    if (verbose) printf("probe_zero_ext -- \
Server responded with alert DECODE_ERROR, i.e. affected.\n");
    ret = EXIT_AFF;
  }
  else if (record.type == SSL_RECORD_TYPE__HANDSHAKE &&
           record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
    if (verbose) printf("probe_zero_ext -- \
Server responded ServerHello, i.e. not affected.\n");
    ret = EXIT_NOT_AFF;
  }
  else {
    printf("probe_zero_ext -- Server responded with the following unexpected record:");
    print_SSL_record_info(record);
    ret = EXIT_UNKNOWN_FAILURE;
  }
  
  send_close_notify(sock, record);
 
  free(clienthello_data_mod);
  free(rand_bytes);
  free(epoch_time);
  
  return ret;
}