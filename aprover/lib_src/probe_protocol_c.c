//
//  probe_protocol.c
//  aprover
//
//  Created by Martin Andersson on 11/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//


/*
 * Probes for the SSL/TLS protocol supported.
 * Loaded by probe_protocol.cpp
 * Connection setup is based on the script from the OpenSSL wiki page
 * https://wiki.openssl.org/index.php/SSL/TLS_Client
 */


#include "c_probe_utils.h"

// Probing technique using the OpenSSL C API
int c_probe(const char* host, unsigned char protocol, int verbose)
{
  int ret = 0;
 
  if (verbose) printf("probe_protocol.c host in: %s, protocol in: 0x%02x\n", host, protocol);
  
  long res = 1;
  unsigned long ssl_err = 0;

  SSL_CTX* ctx = NULL;
  BIO *sbio = NULL, *out = NULL;
  
  SSL *ssl = NULL;
  
  do {
    // Internal function that wraps the OpenSSL init's  
    init_openssl_library();
    
    const SSL_METHOD* method = NULL;
    if (protocol == SSL_MINOR_VERSION__UNKNOWN)
      method = SSLv23_method();
    else if (protocol == SSL_MINOR_VERSION__SSL3)
      method = SSLv3_client_method();
    else if (protocol == SSL_MINOR_VERSION__TLS1_0)
      method = TLSv1_client_method();
    else if (protocol == SSL_MINOR_VERSION__TLS1_1)
      method = TLSv1_1_client_method();
    else if (protocol == SSL_MINOR_VERSION__TLS1_2)
      method = TLSv1_2_client_method();
    else {
      printf("--- Undefined protocol given: 0x%02x\n", protocol);
      exit(20);
    }
    
    ssl_err = ERR_get_error();
    assert(NULL != method);
    if(!(NULL != method)) {
      print_ssl_error_string(ssl_err, "SSLv23_method");
      break; // failed
    }
    
    ctx = SSL_CTX_new(method);
    ssl_err = ERR_get_error();
    assert(ctx != NULL);
    if(!(ctx != NULL)) {
      print_ssl_error_string(ssl_err, "SSL_CTX_new");
      break; // failed
    }
    
    // To remove SSLv2 & 3 use SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3
    // To remove compression use SSL_OP_NO_COMPRESSION
    const long flags = SSL_OP_ALL ;
    long old_opts = SSL_CTX_set_options(ctx, flags);
    (void)old_opts; // Unused variable
    
    
    sbio = BIO_new_ssl_connect(ctx);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_new_ssl_connect\n");
    assert(sbio != NULL);
    if(!(sbio != NULL)) {
      print_ssl_error_string(ssl_err, "BIO_new_ssl_connect");
      ret = EXIT_CONNECT_FAILURE;
      break; // failed
    }
    
    res = BIO_set_conn_hostname(sbio, host);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_set_conn_hostname\n");
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_set_conn_hostname");
      ret = EXIT_CONNECT_FAILURE;
      break; // failed
    }
    
   // This copies an internal pointer. No need to free.
    BIO_get_ssl(sbio, &ssl);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_get_ssl\n");
    assert(ssl != NULL);
    if(!(ssl != NULL)) {
      print_ssl_error_string(ssl_err, "BIO_get_ssl");
      break; //
    }
    
    out = BIO_new_fp(stdout, BIO_NOCLOSE);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_new_fp\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    assert(NULL != out);
    if(!(NULL != out)) {
      print_ssl_error_string(ssl_err, "BIO_new_fp");
      break; // failed
    }
    
    res = BIO_do_connect(sbio);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_do_connect\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_do_connect");
      ret = EXIT_CONNECT_FAILURE;
      break; // failed
    }
    
    res = BIO_do_handshake(sbio);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_do_handshake\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_do_handshake");
      ret = EXIT_HANDSHAKE_FAILURE;
      break; // failed
    }
    
    // This is where to perform X509 verification

    // Read and write can now be performed
    
    // Get protocol
    ret = ((ssl->session->ssl_version & 0x03) +1);
    
  } while (0);
  
  
  // Free up memory and close down connection
  if(out)
    BIO_free(out);
  if(sbio != NULL)
    BIO_free_all(sbio);
  if(NULL != ctx)
    SSL_CTX_free(ctx);
  
  return ret;
}
