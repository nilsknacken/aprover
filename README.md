This is a tool and framework developed in C++/C for the Unix environmnet to actively fingerprint OpenSSL implementation versions. It was originally produced for a MSc project within computer science at the University of Bristol.

More information will become available here soon but all you need to you know is already in the thesis.pdf document.


DISCLAIMER: The Author does not take any responsibility and can not be held liable in any way for any use of this software.