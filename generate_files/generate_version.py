__author__ = 'Martin'

def generate_h():
    versionfile = open("version_constants.h", 'w')

    major_enum = ["EIGHT", "ZERO", "ONE", "TWO"]
    major_string = ["0.9.8", "1.0.0", "1.0.1", "1.0.2"]

    versionfile.write('''//
//  version.h
//  aprover
//
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * THIS FILE IS AUTO GENERATED FROM generate_version.py
 * DO NOT ALTER THIS FILE MANUALLY, INSTEAD EDIT THE PYTHON SCRIPT
 */


/*
 * The data structures in this file is used for conversions between
 * readable string representation of OpenSSL versions and the more
 * efficient enum representation used internally in the program.
 */

#ifndef version_constants_h
#define version_constants_h

#include <string>
#include <vector>

// unsigned short is at least 16 bit
// works up until version 1.0.2
using version_t = unsigned short;

// REMEMBER TO RERUN generate_version.py if a new major version is added
const std::string CURRENT_VERSIONS = "0.9.8:0.9.8zg, 1.0.0:1.0.0s, 1.0.1:1.0.1o, 1.0.2:1.0.2c";

// Enum to represent the different versions of OpenSSL
enum class OPENSSL_VERSION : version_t {
\tUNKNOWN = 0,\n''')

    v = 1
    for major in major_enum:
        versionfile.write('\t{0}\t=\t{1},\n' .format(major, v))
        v += 1
        minor = 'A'
        while 'Y' >= minor:
            versionfile.write('\t{0}_{1}\t=\t{2},\n' .format(major, minor, v))
            minor = chr(ord(minor)+1)
            v += 1
        minor = 'A'
        while 'Y' >= minor:
            versionfile.write('\t{0}_Z{1}\t=\t{2},\n' .format(major, minor, v))
            minor = chr(ord(minor)+1)
            v += 1
    versionfile.write('\tMAX\t=\t{0}}};\n\n'.format(v))

    versionfile.write('// Vector to get string representation of the different versions of OpenSSL\n')
    versionfile.write('const std::vector<std::string> OPENSSL_VERSION_name{"unknown", // 0\n')
    v = 1
    for major in major_string:
        versionfile.write('\t"{0}",\t// {1}\n' .format(major, v))
        v += 1
        minor = 'a'
        while 'y' >= minor:
            versionfile.write('\t"{0}{1}",\t// {2}\n' .format(major, minor, v))
            minor = chr(ord(minor)+1)
            v += 1
        minor = 'a'
        while 'y' >= minor:
            versionfile.write('\t"{0}z{1}",\t// {2}\n' .format(major, minor, v))
            minor = chr(ord(minor)+1)
            v += 1
    versionfile.write('\t"max"}}; // {0}\n\n'.format(v))

    versionfile.write('#endif')
    versionfile.close()

