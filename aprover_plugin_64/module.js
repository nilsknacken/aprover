/*
 * Provides functinality specific to this module, such as:
 * sending the current URL to the tool and handling events 
 * in the plugin.
 *
 * Based on the example provided in:  
 * <nacl_sdk_root>/pepper_<version>/examples/tutorial/dlopen
 *
 * Author: Martin Andersson 2015-07-22
 * Original headers are kept below.
 */


// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });
}

function sendActiveURL(callback) {
  getCurrentTabUrl(function(url_text) {
    console.log('sendActiveURL got:' + url_text);
    var adr = url_text.split("/");
    updateURL(adr[0] + "//" + adr[2]);
    if (adr[0] != 'https:') {
      common.updateStatus('Protocol is not https');
      common.logMessage('Error: protocol is not https');
      common.logMessage('The tool must be run on a site using https (port 443)');
    } else {
      common.naclModule.postMessage('url:'+adr[2]); 
      callback();
    } 
  });
}

function updateURL(url) {
    var urlField = document.getElementById('urlField');
    if (urlField) {
      urlField.innerHTML = url;
    }
}

function updateVersion(version) {
    var versionField = document.getElementById('versionField');
    if (versionField) {
      versionField.innerHTML = version;
    }
}

function taxonomyClicked(event) {
  console.log("taxonomy_link clicked");
  var link = "http://tinyurl.com/taxonomy-openssl";
  chrome.tabs.create({url: link});
  event.preventDefault();
}

// Called by the common.js module.
function attachListeners() {
  document.getElementById('taxonomyLink').addEventListener('click', taxonomyClicked);
}

// Called by the common.js module.
function moduleDidLoad() {
  // The module is not hidden by default so we can easily see if the plugin
  // failed to load.
  common.hideModule();
}
