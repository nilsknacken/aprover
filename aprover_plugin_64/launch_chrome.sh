#!/bin/bash

# Launch Chrome with the required flags for NaCl to access the socket api
# Use for the packaged extension
ext_id="hflapaenefajlkelfigfgeglgnldonma"


platform='unknown'
unamestr=`uname`
chrome='/path/to/chrome'
if [[ "$unamestr" == 'Linux' ]]; then
  platform='linux'
  chrome=`which google-chrome`
elif [[ "$unamestr" == 'Darwin' ]]; then
  platform='mac'
  chrome="/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
else
	echo "unknown platform $unamestr"
  exit 1
fi

"${chrome}" --enable-nacl --allow-nacl-socket-api=${ext_id}
